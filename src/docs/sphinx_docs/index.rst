=============================
PETSc |version| Documentation
=============================

.. raw:: html

   <a href="https://my.siam.org/Store/Product/viewproduct/?ProductId=32850137">Ed Bueler, PETSc for Partial Differential Equations: Numerical Solutions in C and Python</a>

   as a <a href="https://play.google.com/store/books/details/Ed_Bueler_PETSc_for_Partial_Differential_Equations?id=tgMHEAAAQBAJ">Google Play E-book</a>

PETSc, pronounced PET-see (`/ˈpɛt-siː/ <https://en.wikipedia.org/wiki/Help:IPA/English#Key>`__), is a suite of
data structures and routines for the scalable (parallel) solution of scientific
applications modeled by partial differential equations. It supports MPI, and GPUs through
CUDA or OpenCL, as well as hybrid MPI-GPU parallelism. PETSc (sometimes called PETSc/Tao)
also contains the Tao optimization software library.

PETSc is developed as :ref:`open-source <doc_license>`, requests and contributions are welcome.

===========
Get Started
===========

.. toctree::
   :maxdepth: 1

   overview/index
   install/index
   faq/index

===========
Get Working
===========

.. toctree::
   :maxdepth: 1

   manual/index
   guides/guide_to_examples


* `Manual Pages <docs/index.html>`__
* `Function Index <docs/manualpages/singleindex.html>`__
* `Examples Index <docs/manualpages/help.html>`__
* `Strategies for using Fortran <docs/manualpages/Sys/UsingFortran.html>`__

============
Get Involved
============

.. toctree::
   :maxdepth: 1

   contact/index
   developers/index
   miscellaneous/index

.. raw:: html

    <a href="../../../src/binding/petsc4py/docs/usrman/index.html">PETSc4py documentation</a>


.. todolist::
